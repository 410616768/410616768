﻿CREATE TABLE [dbo].[Table]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [name] NVARCHAR(10) NOT NULL, 
    [Email] NVARCHAR(50) NOT NULL, 
    [password] NVARCHAR(10) NOT NULL, 
    [birthday] DATETIME2 NOT NULL, 
    [gender] BIT NOT NULL
)
