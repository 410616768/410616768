﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.ViewModles
{
   
   public class BMIData
  {
        [Required(ErrorMessage ="required field") ]
        [Range(30,150,ErrorMessage="30~50")]
       public float Weight { get; set; }
        [Required(ErrorMessage = "required field")]
        [Range(30, 150, ErrorMessage = "30~50")]
        public float Height { get; set; }
       public float BMI { get; set; }
       public string Level { get; set; }
    }
}
    
