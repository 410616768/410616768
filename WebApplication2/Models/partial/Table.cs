﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    [MetadataType(typeof(TableMetadata))]
    public class TablePartial
    {
    }

    public class TableMetadata
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string Email { get; set; }
        public string password { get; set; }
        public Nullable<System.DateTime> birthday { get; set; }
        public Nullable<bool> gender { get; set; }
    }
    public partial class Table
    {
    }
    public class TableMetadata
    {
        [MetadataType(typeof(UsersMetadata))]
        public partial class Users
        {
        }

        public class UsersMetadata
        {
            public int Id { get; set; }

            [Required(ErrorMessage = "必填欄位")]
            [DisplayName("姓名")]
            [StringLength(10)]
            public string Name { get; set; }

            [Required(ErrorMessage = "必填欄位")]
            [DisplayName("Email")]
            [StringLength(50)]
            [EmailAddress]
            public string Email { get; set; }

            [Required(ErrorMessage = "必填欄位")]
            [DisplayName("密碼")]
            [StringLength(10)]
            public string Password { get; set; }

            [Required(ErrorMessage = "必填欄位")]
            [DisplayName("出生年月日")]
            public System.DateTime Birthday { get; set; }

            [Required(ErrorMessage = "必填欄位")]
            [DisplayName("性別")]
            public bool Gender { get; set; }
        }

    }
}
